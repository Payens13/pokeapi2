package com.example.pokeapi

import android.telecom.Call
import androidx.compose.ui.graphics.Path
import retrofit2.http.GET
import retrofit2.http.Headers


interface PokeAPI {
    @Headers("Accept: application/json")
    // Método para obtener todos los pokemon
    @GET("pokemon")
    fun getPokemons(): Call<PokemonResponse>
    // Método para obtener una pokemon por su ID
    @GET("pokemon/{id}")
    fun getPokemon(@Path("id") id: Int): Call<Pokemon>
}

