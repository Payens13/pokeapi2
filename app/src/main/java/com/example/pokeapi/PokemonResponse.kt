package com.example.pokeapi

data class PokemonResponse (
    @SerializedName("results") var results: ArrayList<Pokemon>
) {
    annotation class SerializedName(val value: String)
}